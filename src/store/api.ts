import { defineStore } from 'pinia';

export const useApi = defineStore('api', {
  state: () => {
    return {
      requests: {} as Record<string, AbortController>
    }
  },
})
